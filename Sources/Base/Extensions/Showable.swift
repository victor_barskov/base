//
//  Showable.swift
//  Prisma
//
//  Created by Victor Barskov on 15.10.2021.
//  Copyright © 2021 Medialogia. All rights reserved.
//

import UIKit

public protocol Showable: AnyObject {
    func show()
    func dismiss(with animation: Bool, completion: (() -> Swift.Void)?)
}

public extension Showable where Self: UIView {
    
    func show() {
        if var topController = UIApplication.topViewController() {
            if let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            topController.view.addSubview(self)
        }
    }
    
    func showOnMain() {
        UIApplication.shared.appWindow?.addSubview(self)
        UIApplication.shared.appWindow?.bringSubviewToFront(self)
    }
    
    func showOn(controller: UIViewController) {
        controller.view.addSubview(self)
    }
    
    func showOn(navController: UINavigationController) {
        navController.view.addSubview(self)
    }
    
    func dismiss(with animation: Bool, completion: (() -> Swift.Void)?) {
        if !animation {
            self.removeFromSuperview()
            completion?()
            return
        }
        UIView.animate(withDuration: 0.22, animations: {
            self.alpha = 0
        }) { (success) in
            self.removeFromSuperview()
            completion?()
        }
    }
        
}
