//
//  ViperInitializer.swift
//
//  Created by vbarskov on 02.10.2019.
//

import Foundation
import UIKit

public protocol ViperModuleInitializer: AnyObject {
    var viewController: UIViewController? { get }
}

public protocol ViperInitializer: AnyObject {
    func createModule(values: [Any?]?)
}

public extension ViperModuleInitializer where Self: NSObject {
    static func assemble(values: Any?...) -> Self {
        let instance = Self.init()
        if let instance = instance as? ViperInitializer {
            instance.createModule(values: values)
        } else {
            instance.awakeFromNib()
        }
        return instance
    }
    
    var viewController: UIViewController? {
        return value(forKey: "viewController") as? UIViewController
    }
}
