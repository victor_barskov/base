//
//  Closurable.swift
//  Salecatcher
//
//  Created by Victor Barskov on 03.01.2022.
//

import Foundation

public protocol Closurable: AnyObject {}
// restrict protocol to only classes => can refer to the class instance in the protocol extension
public extension Closurable {

  // Create container for closure, store it and return it
    func getContainer(for closure: @escaping (Self) -> Void) -> ClosureContainer<Self> {
    weak var weakSelf = self
    let container = ClosureContainer(closure: closure, caller: weakSelf)
    // store the container so that it can be called later, we do not need to explicitly retrieve it.
    objc_setAssociatedObject(self, Unmanaged.passUnretained(self).toOpaque(), container as AnyObject?, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    return container
  }
}

/// Container class for closures, so that closure can be stored as an associated object
public final class ClosureContainer<T: Closurable> {

  var closure: (T) -> Void
  var caller: T?

  init(closure: @escaping (T) -> Void, caller: T?) {
    self.closure = closure
    self.caller = caller
  }

  // method for the target action, visible to UIKit classes via @objc
  @objc func processHandler() {
    if let caller = caller {
      closure(caller)
    }
  }

  // target action
  var action: Selector { return #selector(processHandler) }
}
