//
//  UITableViewCell+.swift
//  Prizma_Arm
//
//  Created by Victor Barskov on 25.01.2022.
//

import UIKit

public extension UITableViewCell {
    class var identifier: String { return String.className(self) }
}
