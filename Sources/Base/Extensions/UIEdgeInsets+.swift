//
//  UIEdgeInsets+.swift
//  BNPL
//
//  Created by Victor Barskov on 14.01.2022.
//

import UIKit

public extension UIEdgeInsets {
    func inverted() -> UIEdgeInsets {
        return UIEdgeInsets(top: -top, left: -left,
                            bottom: -bottom, right: -right)
    }
}
