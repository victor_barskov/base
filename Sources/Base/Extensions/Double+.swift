//
//  File.swift
//  
//
//  Created by Victor Barskov on 02.04.2022.
//

import Foundation

public extension Double  {
    /// Rounds the double to decimal places value
        func roundToPlaces(_ places:Int) -> Double {
            let divisor = pow(10.0, Double(places))
            return (self * divisor).rounded() / divisor
        }
        func cutOffDecimalsAfter(_ places:Int) -> Double {
            let divisor = pow(10.0, Double(places))
            return (self*divisor).rounded(.towardZero) / divisor
        }
}
