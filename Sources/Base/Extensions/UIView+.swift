//
//  File.swift
//  
//
//  Created by Victor Barskov on 31.01.2022.
//

import UIKit

public extension UIView {
    
    func addShadow(opacity: CGFloat = 0.33, whiteBackground: Bool = false) {
        if whiteBackground {
            setShadow(size: CGSize(width: 0, height: 3), opacity: 0.1, radius: 3)
        } else {
            setShadow(size: CGSize(width: 0, height: 5), opacity: opacity, radius: 4, color: .black)
        }
    }
    
    func setShadow(size: CGSize, opacity: CGFloat, radius: CGFloat, color: UIColor = .black) {
        layer.shadowColor   = color.cgColor
        layer.shadowOffset  = size
        layer.shadowOpacity = Float(opacity)
        layer.shadowRadius  = radius
        layer.masksToBounds = false
    }
    
    func addInnerShadow() {
        let innerShadow = CALayer()
        innerShadow.frame = bounds
        
        // Shadow path (1pt ring around bounds)
        let radius = self.layer.cornerRadius
        let path = UIBezierPath(roundedRect: innerShadow.bounds.insetBy(dx: 2, dy:2), cornerRadius:radius)
        let cutout = UIBezierPath(roundedRect: innerShadow.bounds, cornerRadius:radius).reversing()
        
        path.append(cutout)
        innerShadow.shadowPath = path.cgPath
        innerShadow.masksToBounds = true
        
        // Shadow properties
        innerShadow.shadowColor = UIColor.black.cgColor
        innerShadow.shadowOffset = CGSize(width: 0, height: 0)
        innerShadow.shadowOpacity = 0.5
        innerShadow.shadowRadius = 2
        innerShadow.cornerRadius = self.layer.cornerRadius
        layer.addSublayer(innerShadow)
    }
    
    
    func changeShadowBy(highlighted isHighlighted: Bool, opacity: Float = 0.33, whiteBackground: Bool = false) {
        if whiteBackground {
            layer.shadowOpacity = isHighlighted ? 0 : 0.1
        } else {
            layer.shadowOpacity = isHighlighted ? 0 : opacity
        }
    }
    
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        if #available(iOS 11.0, *) {
            clipsToBounds = true
            layer.cornerRadius = radius
            layer.maskedCorners = CACornerMask(rawValue: corners.rawValue)
        } else {
            let path = UIBezierPath(
                roundedRect: bounds,
                byRoundingCorners: corners,
                cornerRadii: CGSize(width: radius, height: radius)
            )
            let mask = CAShapeLayer()
            mask.path = path.cgPath
            layer.mask = mask
        }
    }
    
    func snapshot() -> UIImage {
        if #available(iOS 10.0, *) {
            let renderer = UIGraphicsImageRenderer(bounds: self.bounds)
            return renderer.image { (context) in
                self.layer.render(in: context.cgContext)
            }
        } else {
            return UIImage()
        }
        
    }
    
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
    
}
