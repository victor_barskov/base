//
//  PopView.swift
//  Prisma
//
//  Created by Victor Barskov on 15.10.2021.
//  Copyright © 2021 Medialogia. All rights reserved.
//

import UIKit

open class PopView: UIView, Showable, UIGestureRecognizerDelegate {
        
    public var backView                  = UIView()
    public var snap                      : UIImage?
    public var input                     : [Any?]?
        
    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.snap = super.snapshot()
        setup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
      
    
    @objc func showOnViewController(controller: UIViewController) {
        showOn(controller: controller)
    }
    @objc func showOnMainWindow () {
        showOnMain()
    }
    
    public override func willMove(toSuperview newSuperview: UIView?) { }
    public override func didMoveToSuperview() { }
    deinit {}
    
}

public extension PopView {
        
    func setup() {
        
        addSubview(backView)
        
        translatesAutoresizingMaskIntoConstraints = true
        backView.translatesAutoresizingMaskIntoConstraints = true
        backView.frame = self.frame
        backView.with {
            $0.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
            $0.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
            $0.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
            $0.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        }
        
        let snapshotView = UIImageView(image: self.snap ?? UIImage())
        
        let effectView = UIVisualEffectView()
        effectView.frame = snapshotView.frame
        snapshotView.addSubview(effectView)
        effectView.alpha = 0.0
        effectView.backgroundColor = .black
        snapshotView.alpha = 0.97
        
        backView.addSubview(snapshotView)

        snapshotView.translatesAutoresizingMaskIntoConstraints = true

        snapshotView.with {
            $0.leadingAnchor.constraint(equalTo: backView.leadingAnchor).isActive = true
            $0.topAnchor.constraint(equalTo: backView.topAnchor).isActive = true
            $0.trailingAnchor.constraint(equalTo: backView.trailingAnchor).isActive = true
            $0.bottomAnchor.constraint(equalTo: backView.bottomAnchor).isActive = true
        }
        
        UIView.animate(withDuration: 0.22, animations: {
            effectView.alpha = 0.6
        }) { (success) in
            
        }
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTapBackView))
        tapGestureRecognizer.delegate = self
        backView.addGestureRecognizer(tapGestureRecognizer)
        
        layoutIfNeeded()
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if let touchedView = touch.view, let gestureView = gestureRecognizer.view, touchedView.isDescendant(of: gestureView), touchedView !== gestureView {
            return false
        }
        return true
    }
    
    @objc private func didTapBackView() {
        self.dismiss(with: true, completion: nil)
    }
    
}




