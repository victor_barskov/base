//
//  With.swift
//  Prizma_Arm
//
//  Created by Victor Barskov on 25.01.2022.
//

import Foundation

public protocol With {}

public extension With where Self: Any {
    @discardableResult
    func with(_ block: (Self) -> Void) -> Self {
        block(self)
        return self
    }
}

extension NSObject: With {}
