//
//  UIButton+.swift
//  BNPL
//
//  Created by Victor Barskov on 14.01.2022.
//

import UIKit

public extension UIButton {
    func setTitleWithoutAnimation(title: String?, state: UIControl.State = .normal) {
        UIView.setAnimationsEnabled(false)
        
        setTitle(title, for: state)
        
        layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
    }
    
}
