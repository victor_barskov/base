//
//  Alert.swift
//  carpets
//
//  Created by Victor Barskov on 29.01.2022.
//

import UIKit

public class AlertView: PopView {
    
    let back = UIView(frame: .zero)
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setupUI() {
        addSubview(back)
        translatesAutoresizingMaskIntoConstraints = false
        back.translatesAutoresizingMaskIntoConstraints = false
        back.with {
            $0.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
            $0.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
            $0.heightAnchor.constraint(equalToConstant: 20).isActive = true
            $0.widthAnchor.constraint(equalToConstant: 20).isActive = true
        }
        back.backgroundColor = .red
        layoutIfNeeded()
        
    }
    
}


