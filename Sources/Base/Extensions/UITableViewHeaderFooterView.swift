//
//  UITableViewHeaderFooterView.swift
//  BNPL
//
//  Created by Victor Barskov on 27.01.2022.
//

import UIKit

public extension UITableViewHeaderFooterView {
    class var identifier: String { return String.className(self) }
}


