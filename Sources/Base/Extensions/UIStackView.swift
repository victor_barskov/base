//
//  UIStackView.swift
//  BNPL
//
//  Created by Victor Barskov on 19.01.2022.
//

import UIKit

public extension UIStackView {
    func removeAllArrangedSubviews() {
        for subview in arrangedSubviews {
            removeArrangedSubview(subview)
            subview.removeFromSuperview()
        }
    }
}
