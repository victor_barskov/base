//
//  ViperModuleTransitionHandler.swift
//
//  Created by vbarskov on 02.10.2019.
//

import Foundation
import UIKit

@objc public enum TransitionStyle: Int {
    case push = 0
    case modal
    case modalRootNavBar
    case modalRootNavBarFullScreen
    case modalFullScreen
}



public protocol ViperModuleTransitionHandler: AnyObject {
    func openModule(segueIdentifier: String)
    func openModule(vc: UIViewController, style: TransitionStyle, completion: (() -> Swift.Void)?)
    func openModule(vc: UIViewController, style: TransitionStyle, animated: Bool, completion: (() -> Swift.Void)?)
    func openModule(transitionHandler: ViperModuleTransitionHandler, style: TransitionStyle)
    func openModuleAsRoot(for vc: UIViewController?, with transition: UIView.AnimationOptions?, duration: CGFloat, completion: (()->())?)
    func closeModule()
    func closeModule(animated: Bool)
    func closeModule(animated: Bool, showNewRoot: (()->())?)
    func popModule()
    func popModule(animated: Bool)
}

public extension ViperModuleTransitionHandler {
    func openModule(vc: UIViewController, style: TransitionStyle,  completion: (() -> Swift.Void)? = nil) {
        self.openModule(vc: vc, style: style, completion: completion)
    }
}

public extension ViperModuleTransitionHandler where Self: UIViewController {
    
    func openModuleAsRoot(for vc: UIViewController?, with transition: UIView.AnimationOptions?, duration: CGFloat, completion: (()->())?) {
        if let window = UIApplication.shared.delegate?.window {
            if let w = window {
                UIView.transition(with: w, duration: 0.33, options:transition ?? .transitionFlipFromLeft, animations: {
                    if let vc = vc {w.rootViewController = vc}
                }, completion: { completed in
                    completion?()
                })
            }
        }
    }

    func openModule(segueIdentifier: String) {
        performSegue(withIdentifier: segueIdentifier, sender: nil)
    }
    
    func openModule(vc: UIViewController, style: TransitionStyle, completion: (() -> Swift.Void)? = nil) {
        switch style {
        case .modal:
            self.present(vc, animated: true, completion: completion)
        case .modalRootNavBar:
            let nav = UINavigationController(rootViewController: vc)
            self.present(nav, animated: true, completion: completion)
        case .modalRootNavBarFullScreen:
            let nav = UINavigationController(rootViewController: vc)
            nav.modalPresentationStyle = .fullScreen
            self.present(nav, animated: true, completion: nil)
        case .modalFullScreen:
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: completion)
        case .push:
            self.navigationController?.pushViewController(vc, animated: true)
            do {
                completion?()
            }
        }
    }
    
    func openModule(vc: UIViewController, style: TransitionStyle, animated: Bool, completion: (() -> Swift.Void)? = nil) {
        switch style {
        case .modal:
            self.present(vc, animated: animated, completion: completion)
        case .modalRootNavBar:
            let nav = UINavigationController(rootViewController: vc)
            self.present(nav, animated: animated, completion: completion)
        case .modalRootNavBarFullScreen:
            let nav = UINavigationController(rootViewController: vc)
            nav.modalPresentationStyle = .fullScreen
            self.present(nav, animated: true, completion: nil)
        case .modalFullScreen:
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: animated, completion: completion)
        case .push:
            self.navigationController?.pushViewController(vc, animated: animated)
            do {
                completion?()
            }
        }
    }
    
    func openModule(transitionHandler: ViperModuleTransitionHandler, style: TransitionStyle) {
        guard let vc = transitionHandler as? UIViewController else {
            return
        }
        switch style {
        case .modal:
            self.present(vc, animated: true, completion: nil)
        case .modalRootNavBar:
            let nav = UINavigationController(rootViewController: vc)
            self.present(nav, animated: true, completion: nil)
        case .modalRootNavBarFullScreen:
            let nav = UINavigationController(rootViewController: vc)
            nav.modalPresentationStyle = .fullScreen
            self.present(nav, animated: true, completion: nil)
        case .modalFullScreen:
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        case .push:
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func closeModule() {
        if presentingViewController != nil {
            dismiss(animated: true, completion: nil)
        } else {
            let animated = presentedViewController == nil
            navigationController?.popToRootViewController(animated: animated)
        }
    }
    
    func closeModule(animated: Bool) {
        if presentingViewController != nil {
            dismiss(animated: animated, completion: nil)
        } else {
            let animated = presentedViewController == nil
            navigationController?.popToRootViewController(animated: animated)
        }
    }
    
    func closeModule(animated: Bool, showNewRoot: (()->())?) {
        if presentingViewController == nil && presentedViewController == nil {
            showNewRoot?()
        }
        if presentingViewController != nil {
            dismiss(animated: animated, completion: nil)
        } else {
            let animated = presentedViewController == nil
            navigationController?.popToRootViewController(animated: animated)
        }
    }
    
    func popModule() {
        if presentingViewController != nil {
            dismiss(animated: true, completion: nil)
        } else {
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    func popModule(animated: Bool) {
        if presentingViewController != nil {
            dismiss(animated: true, completion: nil)
        } else {
            let animated = presentedViewController == nil
            self.navigationController?.popToRootViewController(animated: animated)
        }
    }

    
}
