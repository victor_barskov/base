//
//  UIViewController+.swift
//  Prizma_Arm
//
//  Created by Victor Barskov on 25.01.2022.
//

import UIKit

public extension UIViewController {
    
    func keyboardAnimationTime(notification: NSNotification) -> Double {
        var result: Double = 0.25
        guard
            let userInfo = notification.userInfo
            else {return result}
        if let animationDuration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double {
            result = animationDuration
        }
        return result
    }
    
    
    func keyboardHeight(notification: NSNotification) -> CGFloat {
        let result: CGFloat = 216
        guard
            let userInfo = notification.userInfo,
            let keyboardEndFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            else { return result}
        return keyboardEndFrame.height
    }
    
    func hideKeyboardOnTap(action: Selector) -> UITapGestureRecognizer {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: action)
        return tap
    }
    
    func hideKeyboardByTap(cancelTouches: Bool=false) {
        
        if let recognizers = view.gestureRecognizers {
          for recognizer in recognizers {
            view.removeGestureRecognizer(recognizer)
          }
        }
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = cancelTouches
        
        // if cancelTouches == false { tap.cancelsTouchesInView = cancelTouches }
        view.addGestureRecognizer(tap)
    }
    
    @objc private func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func resignResponderTap(action: Selector) -> UITapGestureRecognizer {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: action)
        return tap
    }
    
    func hideKeyboard() {
        view.endEditing(true)
    }
}
