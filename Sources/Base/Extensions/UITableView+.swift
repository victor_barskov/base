//
//  UITableView+.swift
//  Prizma_Arm
//
//  Created by Victor Barskov on 25.01.2022.
//

import UIKit

public extension UITableView {
    func register<T: UITableViewCell>(_ classCell: T.Type){
        self.register(T.self, forCellReuseIdentifier: String(describing: T.self))
    }
    
    func reloadDataSameOffset() {
        let offset = contentOffset
        reloadData()
        layoutIfNeeded()
        setContentOffset(offset, animated: false)
    }
}
