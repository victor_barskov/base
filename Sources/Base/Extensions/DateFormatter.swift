//
//  File.swift
//  
//
//  Created by Victor Barskov on 02.04.2022.
//

import Foundation

public extension DateFormatter {
    
    static let normalDate: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.locale = Locale(identifier: "ru")
        formatter.timeZone = TimeZone(abbreviation: "Europe/Moscow")
        formatter.dateFormat = "d MMMM YYYY"
        return formatter
    }()
    
    static let isoDateTimeSec: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.locale = Locale(identifier: "ru")
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        return formatter
    }()
    
    static let isoDateTimeSecMils: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.locale = Locale(identifier: "ru")
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ"
        return formatter
    }()
    
    static let dayTimeFormater: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.locale = Locale(identifier: "ru")
        formatter.dateFormat = "d.HH:mm:ss"
        return formatter
    }()
    
    static let dateTimeForGetRequestFormater: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.locale = Locale(identifier: "ru")
        formatter.timeZone = TimeZone(identifier: "UTC")
        formatter.dateFormat = "yyyyMMdd'T'HHmm"
        return formatter
    }()
    
    static let dateTimeFormater: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.locale = Locale(identifier: "ru")
        formatter.dateFormat = "dd MMMM, HH:mm"
        return formatter
    }()
    
    static let dateTimeWithYearFormater: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.locale = Locale(identifier: "ru")
        formatter.dateFormat = "dd MMMM yyyy HH:mm"
        return formatter
    }()
    
    static let dateFormatterNotLocalized: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.locale = Locale(identifier: "ru")
        formatter.dateFormat = "dd MMMM"
        return formatter
    }()
    
    static let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.locale = .current
        formatter.dateFormat = "dd MMMM"
        return formatter
    }()
    
    static let dateWithYearFormater: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.locale = Locale(identifier: "ru")
        formatter.dateFormat = "dd MMMM yyyy"
        return formatter
    }()
    
    static let fullDateFormater: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.locale = Locale(identifier: "ru")
        formatter.dateFormat = "dd MMMM yyyy HH:mm:ss"
        return formatter
    }()
    
    static let timeFormaterWithSeconds: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.locale = Locale(identifier: "ru")
        formatter.dateFormat = "HH:mm:ss"
        return formatter
    }()
    
    static let timeFormater: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.locale = Locale(identifier: "ru")
        formatter.dateFormat = "HH:mm"
        return formatter
    }()
    
    static let monthDateYearFormater: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.locale = Locale(identifier: "ru")
        formatter.dateFormat = "MMMM dd, yyyy"
        return formatter
    }()
    
    static let weekdayNameFormater: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.locale = Locale(identifier: "ru")
        formatter.dateFormat = "EE"
        return formatter
    }()
    
    static let dayMonthYearTime: DateFormatter = {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "ru")
        formatter.dateFormat = "dd MMMM yyyy HH:mm"
        return formatter
    }()
    
    static let drivingRaitingFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "d MMMM"
        return formatter
    }()
    
    static let lastDealerVisitFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "ru")
        formatter.dateFormat = "dd MMMM, yyyy 'at' HH:mm"
        return formatter
    }()
    
    static let isoDate: DateFormatter = {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "ru")
        formatter.dateFormat = "YYYY-MM-dd"
        return formatter
    }()
    
    
}
