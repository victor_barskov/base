//
//  NSMutableAttributedString.swift
//  Salecatcher
//
//  Created by Victor Barskov on 09.01.2022.
//

import UIKit

public extension NSMutableAttributedString {
    @discardableResult func bold(_ text: String, font: UIFont) -> NSMutableAttributedString {
        let attrs: [NSAttributedString.Key: Any] = [.font: font]
        let boldString = NSMutableAttributedString(string:text, attributes: attrs)
        append(boldString)
        
        return self
    }
    
    @discardableResult func normal(_ text: String) -> NSMutableAttributedString {
        let normal = NSAttributedString(string: text)
        append(normal)
        return self
    }
    
    @discardableResult func normal(_ text: String,  font: UIFont) -> NSMutableAttributedString {
        let attrs: [NSAttributedString.Key: Any] = [.font: font]
        let nString = NSMutableAttributedString(string:text, attributes: attrs)
        append(nString)
        return self
    }
}

public extension NSMutableAttributedString {
    @discardableResult func bold(_ text: String, font: UIFont, color: UIColor) -> NSMutableAttributedString {
        let attrs: [NSAttributedString.Key: Any] = [.font: font, .foregroundColor:color]
        let boldString = NSMutableAttributedString(string:text, attributes: attrs)
        append(boldString)
        
        return self
    }
    
    @discardableResult func normal(_ text: String,  font: UIFont, color: UIColor) -> NSMutableAttributedString {
        let attrs: [NSAttributedString.Key: Any] = [.font: font, .foregroundColor:color]
        let nString = NSMutableAttributedString(string:text, attributes: attrs)
        append(nString)
        return self
    }
    
    @discardableResult func set(_ text: String,  font: UIFont, color: UIColor) -> NSMutableAttributedString {
        let attrs: [NSAttributedString.Key: Any] = [.font: font, .foregroundColor:color]
        let nString = NSMutableAttributedString(string:text, attributes: attrs)
        append(nString)
        return self
    }
}
